Namespace(align_limits=(0.2, 4.2), average=False, config=None, conjugate=False, data='nifti/P004/metab_open.nii.gz', ecc=None, fmrs=True, hlsvd=True, leftshift=2, output='processed/open/P004', overwrite=True, quant='nifti/P004/wref3.nii.gz', reference='nifti/P004/wref1.nii.gz', report=True, t1=None, unlike=False, verbose=False)
--------
Command Line Args:   --data nifti/P004/metab_open.nii.gz --quant nifti/P004/wref3.nii.gz --reference nifti/P004/wref1.nii.gz --output processed/open/P004 --leftshift 2 --hlsvd --fmrs --report --overwrite
Defaults:
  --align_limits:    (0.2, 4.2)
