# Dynamic fitting of diffusion-weighted MRS (dwMRS) data.

This section contains two subsections:

1. `simulation_dti` Explores the use of the dynamic fitting tools to fit simple diffusion models to multi-direction diffusion data. This section used simulated data.
2. `invivo` uses the fsl-mrs dynamic fitting tools to replicates the results of [published work](https://doi.org/10.1016/j.neuroimage.2019.02.046) performing dwMRS in two pre-clinical cohorts undergoing pharmacological modulation.

The first subsection corresponds to `Diffusion-weighted MRS – Simulation`, and the second to `Diffusion-weighted MRS – In vivo` in the paper's _Methods_ and _Results_.