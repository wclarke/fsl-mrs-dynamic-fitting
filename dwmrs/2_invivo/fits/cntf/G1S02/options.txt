{"data": "nifti/cntf/G1S02.nii.gz", "basis": "basis_optim", "output": "fits/cntf/G1S02", "dyn_config": "dmrs_model_biexp_mm_lin.py", "time_variables": ["bvals"], "ppmlim": [0.2, 4.2], "h2o": null, "baseline_order": 1, "metab_groups": ["MM_WT_"], "lorentzian": true, "t1": null, "report": true, "verbose": false, "overwrite": true, "no_rescale": false, "config": null}
--------
Command Line Args:   --data nifti/cntf/G1S02.nii.gz --basis basis_optim --dyn_config dmrs_model_biexp_mm_lin.py --time_variables bvals --lorentzian --baseline_order 1 --metab_groups MM_WT_ --output fits/cntf/G1S02 --report --overwrite
Defaults:
  --ppmlim:          (0.2, 4.2)
